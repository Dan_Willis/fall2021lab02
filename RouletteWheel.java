import java.util.Random;
public class RouletteWheel{

    private Random random;
    private int number;

    public RouletteWheel(){
        this.random = new Random();
        this.number = 0;    
    }

    public void spin(){
        this.number = this.random.nextInt(37);
    }

    public int getValue(){
        return this.number;
    }

}